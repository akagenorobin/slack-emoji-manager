module gitlab.com/akagenorobin/slack-emoji-manager

go 1.12

require (
	github.com/comail/colog v0.0.0-20160416085026-fba8e7b1f46c
	github.com/gorilla/websocket v1.4.0 // indirect
	github.com/jinzhu/gorm v1.9.10
	github.com/joho/godotenv v1.3.0
	github.com/nlopes/slack v0.5.1-0.20190623232825-2891986e2a3e
)
