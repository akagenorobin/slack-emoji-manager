package main

import (
	"github.com/comail/colog"
	"github.com/joho/godotenv"
	"gitlab.com/akagenorobin/slack-emoji-manager/db"
	"gitlab.com/akagenorobin/slack-emoji-manager/service"
	"log"
	"os"
	"strconv"
)

func main() {
	colog.SetDefaultLevel(colog.LDebug)
	colog.SetMinLevel(colog.LTrace)
	colog.SetFormatter(&colog.StdFormatter{
		Colors: true,
		Flag:   log.Ldate | log.Ltime | log.Lshortfile,
	})
	colog.Register()

	err := godotenv.Load()
	if err != nil {
		log.Print("error: " + err.Error())
		return
	}

	debug, err := strconv.ParseBool(os.Getenv("DEBUG"))
	if err != nil {
		debug = false
	}

	db, err := db.Connect(debug)
	if err != nil {
		log.Print("error: " + err.Error())
		return
	}
	defer db.Close()

	err = service.UpdateEmoji(db)
	if err != nil {
		log.Print("error: " + err.Error())
		return
	}

	err = service.NotifyNewEmoji(db, debug)
	if err != nil {
		log.Print("error: " + err.Error())
		return
	}
}
