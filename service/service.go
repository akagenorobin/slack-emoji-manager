package service

import (
	"github.com/jinzhu/gorm"
	"github.com/nlopes/slack"
	"gitlab.com/akagenorobin/slack-emoji-manager/model"
	"log"
	"os"
	"time"
)

const (
	username  = "new_emoji_bot"
	channel   = "#slack-emoji-factory"
	iconEmoji = ":aiko:"
)

// UpdateEmoji : 絵文字情報を更新する
func UpdateEmoji(db *gorm.DB) error {
	api := slack.New(os.Getenv("SLACK_API_TOKEN"))
	emojis, err := api.GetEmoji()
	if err != nil {
		return err
	}

	tx := db.Begin()

	for name, url := range emojis {
		var emoji model.Emoji
		if err := tx.Where(model.Emoji{Name: name}).Assign(model.Emoji{URL: url}).FirstOrCreate(&emoji).Error; err != nil {
			tx.Rollback()
			return err
		}
	}

	return tx.Commit().Error
}

func extractNewEmoji(db *gorm.DB) (*[]model.Emoji, error) {
	var emojis []model.Emoji
	if err := db.Find(&emojis, "created_at > ?", time.Now().Add(-1*time.Hour)).Error; err != nil {
		return nil, err
	}
	return &emojis, nil
}

// NotifyNewEmoji : 新規に登録された絵文字情報を slack に通知する
func NotifyNewEmoji(db *gorm.DB, debug bool) error {
	emojis, err := extractNewEmoji(db)
	if err != nil {
		return err
	}

	var text string
	if len(*emojis) > 0 {
		text = ":new: :arrow_right:"
		for _, emoji := range *emojis {
			text += " :" + emoji.Name + ":"
		}
	} else {
		text = "進捗がないよ"
	}
	log.Print("info: notify message: " + text)

	if debug {
		return nil
	}

	url := os.Getenv("SLACK_WEBHOOK_URL")
	msg := slack.WebhookMessage{
		Username:  username,
		Channel:   channel,
		IconEmoji: iconEmoji,
		Text:      text,
	}
	err = slack.PostWebhook(url, &msg)

	return err
}
