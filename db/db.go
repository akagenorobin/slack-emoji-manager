package db

import (
	"github.com/jinzhu/gorm"
	// mysql driver
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gitlab.com/akagenorobin/slack-emoji-manager/model"
	"log"
	"os"
)

// Connect : DB に接続する
func Connect(debug bool) (*gorm.DB, error) {
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbSchema := os.Getenv("DB_SCHEMA")

	connection := dbUser + ":" + dbPassword + "@tcp(" + dbHost + ":" + dbPort + ")/" + dbSchema + "?charset=utf8mb4&parseTime=true&loc=Local"
	db, err := gorm.Open("mysql", connection)
	if err != nil {
		return nil, err
	}

	db.LogMode(debug)
	db.Set("gorm:table_options", "CHARSET=utf8mb4").AutoMigrate(&model.Emoji{})

	log.Print("info: connected: " + connection)
	return db, nil
}
