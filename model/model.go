package model

import (
	"github.com/jinzhu/gorm"
)

// Emoji : 絵文字
type Emoji struct {
	gorm.Model
	Name string `gorm:"unique;not null"`
	URL  string `gorm:"not null;size:1000"`
}
